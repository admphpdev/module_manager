<?php

namespace amd_php_dev\module_manager;

/**
 * manager module definition class
 */
class Module extends \amd_php_dev\yii2_components\modules\Module
{
    //public $layout      = '@app/views/layouts/default';
    use \amd_php_dev\yii2_components\modules\ComposerModuleTrait;

    protected $_urlRules = [
        [
            'rules' => [
                ['class' => '\amd_php_dev\module_manager\urlRules\Rules']
            ],
            'append' => true,
        ],
    ];
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'amd_php_dev\module_manager\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->modules = [
            'admin' => [
                'class' => 'amd_php_dev\module_manager\modules\admin\Module',
            ],
        ];

        // custom initialization code goes here
    }

//    public static function getMenuItems() {
//        return [
//            'section' => 'manager',
//            'items' => [
//                [
//                    'label' => 'manager',
//                    'items' => [
//                        ['label' => 'label', 'url' => ['']],
//                    ]
//                ]
//            ],
//        ];
//    }
}
