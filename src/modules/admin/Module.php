<?php

namespace amd_php_dev\module_manager\modules\admin;

/**
 * admin module definition class
 */
class Module extends \amd_php_dev\yii2_components\modules\Admin
{
    //public $layout      = '@app/views/layouts/default';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'amd_php_dev\module_manager\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        //$this->modules = [
        //
        //];

        // custom initialization code goes here
    }

    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                [
                    'label' => 'Менеджеры',
                    'items' => [
                        ['label' => 'Статьи', 'url' => ['/manager/admin/article/index']],
                        ['label' => 'Категории', 'url' => ['/manager/admin/category/index']],
                    ]
                ]
            ],
        ];
    }
}
