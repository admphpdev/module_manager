<?php

namespace amd_php_dev\module_manager\models;

/**
 * This is the ActiveQuery class for [[ManagerCategory]].
 *
 * @see ManagerCategory
 */
class ManagerCategoryQuery extends \amd_php_dev\yii2_components\models\PageQuery
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::behaviors(),
            [
                'nestedCategoryQuery' => [
                    'class' => \amd_php_dev\yii2_components\behaviors\nestedsets\NestedSetsQueryBehavior::className(),
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     * @return ManagerCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ManagerCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
