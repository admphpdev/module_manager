<?php

namespace amd_php_dev\module_manager\models;

/**
 * This is the ActiveQuery class for [[ManagerArticleImage]].
 *
 * @see ManagerArticleImage
 */
class ManagerArticleImageQuery extends \amd_php_dev\yii2_components\models\gallery\ImageGalleryItemQuery
{

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        //return ArrayHelper::merge(parent::behaviors(), [
        //
        //]);
        return parent::behaviors();
    }

    /**
     * @inheritdoc
     * @return ManagerArticleImage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ManagerArticleImage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
