<?php

namespace amd_php_dev\module_manager\models;

/**
 * This is the ActiveQuery class for [[ManagerArticleVideo]].
 *
 * @see ManagerArticleVideo
 */
class ManagerArticleVideoQuery extends \amd_php_dev\yii2_components\models\gallery\VideoGalleryItemQuery
{

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        //return ArrayHelper::merge(parent::behaviors(), [
        //
        //]);
        return parent::behaviors();
    }

    /**
     * @inheritdoc
     * @return ManagerArticleVideo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ManagerArticleVideo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
