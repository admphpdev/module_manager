<?php

namespace amd_php_dev\module_manager\models;

use Yii;

/**
 * This is the model class for table "{{%manager_category}}".
 *
 * @property integer $id_parent
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $tree
 * @property integer $active
 * @property integer $priority
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author
 * @property string $name
 * @property string $name_small
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $text_small
 * @property string $text_full
 * @property string $links
 * @property string $snipets
 * @property string $image_small
 * @property string $image_full
 */
class ManagerCategory extends \amd_php_dev\yii2_components\models\Page
{

    const IMAGES_URL_ALIAS = '@web/data/images/manager/category/';

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::behaviors(),
            [
                'nestedCategory' => [
                    'class' => \amd_php_dev\yii2_components\behaviors\NestedCategoryBehavior::className(),
                    'treeAttribute' => 'tree',
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%manager_category}}';
    }


    /**
    * @inheritdoc
    */
    public static function getActiveArray()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::getActiveArray(), [
        //
        //]);
        return parent::getActiveArray();
    }

    /**
    * @inheritdoc
    */
    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        //return Url::to(['', 'url' => $this->url]);
        return '';
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_parent':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_SELECT;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_parent':
                $result = $this->getBehavior('nestedCategory')->getParentInputData();
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            ['id_parent', 'integer'],
            ['id_parent', 'default', 'value' => 0],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'id_parent' => 'Родитель'
        ]);
    }

    /**
     * @inheritdoc
     * @return ManagerCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ManagerCategoryQuery(get_called_class());
    }

    public function getParentRelation()
    {
        return $this->hasOne(static::className(), ['id' => 'id_parent']);
    }
}
