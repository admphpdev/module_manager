<?php

namespace amd_php_dev\module_manager\models;

/**
 * This is the ActiveQuery class for [[ManagerArticle]].
 *
 * @see ManagerArticle
 */
class ManagerArticleQuery extends \amd_php_dev\yii2_components\models\PageQuery
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'taggableBehavior' => [
                'class' => \amd_php_dev\yii2_components\behaviors\taggable\TaggableQueryBehavior::className()
            ],
        ]);
    }

    /**
     * @inheritdoc
     * @return ManagerArticle[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ManagerArticle|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
