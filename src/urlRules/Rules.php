<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 30.11.15
 * Time: 16:14
 */

namespace amd_php_dev\module_manager\urlRules;

use yii\web\UrlRuleInterface;
use yii\base\Object;
use app\modules\page\models\Page;

class Rules extends Object implements UrlRuleInterface
{
    const MODULE_BASE_URL = 'kontakty';

    public function createUrl($manager, $route, $params)
    {
        if ($route === 'manager/default/index' && isset($params['url'])) {
            return self::MODULE_BASE_URL . '/' . $params['url'];
        }

        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if ($pathInfo == 'manager/default/index') {
            \yii::$app->getResponse()->redirect('/' . self::MODULE_BASE_URL, 301);
            \yii::$app->end();
        }

        $search = preg_quote(\yii::$app->params['HOST'], '/');
        if (preg_match("/^(manager-\d+)\.$search$/", $request->hostName, $matches)) {
            if ($pathInfo == '') {
                return ['manager/default/profile', ['manager' => $matches[1]]];
            }
        } elseif($request->hostName == \yii::$app->params['HOST']) {
            $search = preg_quote(self::MODULE_BASE_URL, '/');
            if (preg_match("/^$search(:?\/([\S]+))?$/", $pathInfo, $matches)) {
                if (empty($matches[1])) {
                    return ['manager/default/index', []];
                }
            }
        }

        return false;  // this rule does not apply
    }
}