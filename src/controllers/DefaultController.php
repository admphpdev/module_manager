<?php

namespace amd_php_dev\module_manager\controllers;

use amd_php_dev\yii2_components\controllers\PublicController;

/**
 * Default controller for the `manager` module
 */
class DefaultController extends PublicController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
