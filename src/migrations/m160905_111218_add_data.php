<?php

use yii\db\Migration;

class m160905_111218_add_data extends Migration
{
    public function safeUp()
    {
        // Роль журналист
        $model = new \amd_php_dev\module_user\models\UserRole();
        $model->name = 'Менеджер';
        $model->role = 'manager';
        $model->parent = \amd_php_dev\module_user\models\User::ROLE_USER;
        $model->active = \amd_php_dev\module_user\models\User::ACTIVE_ACTIVE;
        $model->save();

        $userRole = \Yii::$app->authManager->getRole(\amd_php_dev\module_user\models\User::ROLE_USER);

        $role = \Yii::$app->authManager->createRole($model->role);
        $role->description = $model->name;
        \Yii::$app->authManager->add($role);
        \Yii::$app->authManager->addChild($role, $userRole);
    }

    public function safeDown()
    {
        echo "m160905_111218_add_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
