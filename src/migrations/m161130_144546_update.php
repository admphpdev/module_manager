<?php

use yii\db\Migration;
use \amd_php_dev\module_user\models\UserOptionGroup;
use \amd_php_dev\module_user\models\UserOption;
use \amd_php_dev\module_user\models\UserRole;

class m161130_144546_update extends Migration
{
    public function up()
    {
        if (!$role = UserRole::find()->where(['role' => 'manager'])->one()) {
            return false;
        }

        if (!$group = UserOptionGroup::find()->where(['code' => 'manager_info'])->one()) {
            $group = new UserOptionGroup();
            $group->name = 'Информация о менеджере';
            $group->code = 'manager_info';
            $group->roles = [$role->id];
            $group->active = UserOptionGroup::ACTIVE_ACTIVE;
            $group->save();
        }

        if (!$option = UserOption::find()->where(['code' => 'url'])->one()) {
            $option = new UserOption();
            $option->code = 'url';
            $option->name = 'URL';
            $option->active = UserOption::ACTIVE_ACTIVE;
            $option->id_group = $group->id;
            $option->roles = [$role->id];
            $option->save();
        }

        if (!$option = UserOption::find()->where(['code' => 'title'])->one()) {
            $option = new UserOption();
            $option->code = 'title';
            $option->name = 'Заголовок title';
            $option->active = UserOption::ACTIVE_ACTIVE;
            $option->id_group = $group->id;
            $option->roles = [$role->id];
            $option->save();
        }

        if (!$option = UserOption::find()->where(['code' => 'h1'])->one()) {
            $option = new UserOption();
            $option->code = 'h1';
            $option->name = 'Заголовок h1';
            $option->active = UserOption::ACTIVE_ACTIVE;
            $option->id_group = $group->id;
            $option->roles = [$role->id];
            $option->save();
        }
        $option = null;
    }

    public function down()
    {
        UserOption::deleteAll(['code' => ['url', 'title', 'h1']]);

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
