<?php

use yii\db\Migration;
use \amd_php_dev\module_user\models\UserOptionGroup;
use \amd_php_dev\module_user\models\UserOption;
use \amd_php_dev\module_user\models\UserRole;

class m160906_105306_update_data extends Migration
{
    public function up()
    {
        if (!$role = UserRole::find()->where(['role' => 'manager'])->one()) {
            return false;
        }
        
        if (!$group = UserOptionGroup::find()->where(['code' => 'manager_info'])->one()) {
            $group = new UserOptionGroup();
            $group->name = 'Информация о менеджере';
            $group->code = 'manager_info';
            $group->roles = [$role->id];
            $group->active = UserOptionGroup::ACTIVE_ACTIVE;
            $group->save();
        }

        if (!$option = UserOption::find()->where(['code' => 'post'])->one()) {
            $option = new UserOption();
            $option->code = 'post';
            $option->name = 'Должность';
            $option->active = UserOption::ACTIVE_ACTIVE;
            $option->variants = 'менеджер отдела продаж | ведущий специалист отдела продаж | руководитель отдела продаж | руководитель отдела снабжения | ';
            $option->id_group = $group->id;
            $option->roles = [$role->id];
            $option->save();
        }
        $option = null;

        if (!$option = UserOption::find()->where(['code' => 'specialization'])->one()) {
            $option = new UserOption();
            $option->code = 'specialization';
            $option->name = 'Специализация';
            $option->active = UserOption::ACTIVE_ACTIVE;
            $option->id_group = $group->id;
            $option->roles = [$role->id];
            $option->save();
        }
        $option = null;
    }

    public function down()
    {
        UserOption::deleteAll(['code' => ['specialization', 'post']]);
        UserOptionGroup::deleteAll(['code' => 'manager_info']);

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
