<?php

use \amd_php_dev\yii2_components\migrations\generators\Page;
use \amd_php_dev\yii2_components\migrations\generators\GalleryItem;
use \amd_php_dev\yii2_components\migrations\generators\TagRelation;

class m160620_142210_manager_module extends \amd_php_dev\yii2_components\migrations\Migration
{
    public $categoryTableName               = '{{%manager_category}}';
    public $articleTableName                = '{{%manager_article}}';
    public $articleToTagTableName           = '{{%manager_article_to_tag}}';
    public $articleImageTableName           = '{{%manager_article_image_gallery}}';
    public $articleVideoTableName           = '{{%manager_article_video_gallery}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createCategory();
        $this->createArticle();
    }

    public function safeDown()
    {
        $tables = get_class_vars($this);

        foreach ($tables as $table) {
            if (preg_match('/^\S+TableName$/', $table)) {
                $this->dropTable($table);
            }
        }
    }

    /**
     * Создание категорий
     */
    public function createCategory()
    {
        $generator = new Page($this, $this->categoryTableName);
        $generator->additionalColumns['id_parent'] = $this->integer();
        $generator->additionalColumns['lft'] = $this->integer();
        $generator->additionalColumns['rgt'] = $this->integer();
        $generator->additionalColumns['depth'] = $this->integer();
        $generator->additionalColumns['tree'] = $this->integer();
        $generator->addIndex('lft');
        $generator->addIndex('rgt');
        $generator->addIndex('depth');
        $generator->addIndex('tree');
        $generator->addIndex('id_parent');
        $generator->create();
    }
    /**
     * Создание статей
     */
    public function createArticle()
    {
        $generator = new Page($this, $this->articleTableName);
        $generator->additionalColumns['id_category'] = $this->integer();
        $generator->addIndex('id_category');
        $generator->additionalColumns['id_manager'] = $this->integer();
        $generator->addIndex('id_manager');
        $generator->create();

        // Создание таблицы галереии изображений новостей
        $generator = new GalleryItem($this, $this->articleImageTableName);
        $generator->create();

        // Создание таблицы галереии видео новостей
        $generator = new GalleryItem($this, $this->articleVideoTableName);
        $generator->create();

        $generator = new TagRelation($this, $this->articleToTagTableName);
        $generator->create();
    }
}
