<?php

use yii\db\Migration;

class m160905_101440_add_data extends Migration
{
    public $pageTableName = '{{%page}}';

    public function up()
    {
        $this->insert(
            $this->pageTableName,
            [
                'active' => \amd_php_dev\yii2_components\models\Page::ACTIVE_ARCHIVE,
                'name' => 'Менеджеры',
                'url' => \amd_php_dev\yii2_components\helpers\TextHelper::str2url('Менеджеры'),
                'text_full' => 'Добро пожаловать в список менеджеров',
                'meta_title' => 'Менеджеры'
            ]
        );
    }

    public function down()
    {
        echo "m160905_101440_add_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
